# Manufacturing Links


## Live/videos

https://instrumental.com/resources/teardown/
https://instrumental.com/resources/teardown/change-notice-ps5-controller-teardown/



Make Works videos

CAT scan people.
Lego minifigs
https://scanofthemonth.com/LEGO-Minifig/


Whats inside a robotic KITTEN?
https://www.youtube.com/watch?v=zcX_H8X6ga0

M3 Design Teardowns Playlist
https://www.youtube.com/playlist?list=PLX_QtVuugdKFaOlFUKwkSPV0pLQuagoyq
> Our rigorous teardown process carefully catalogs a purposeful product deconstruction. This method of obtaining indirect development experience is an important tool we use to deliver unique solutions for our clients.

Framework Laptop Teardown
https://www.ifixit.com/News/51614/framework-laptop-teardown-10-10-but-is-it-perfect
> You might not remember earlier this year when a small new company called Framework announced a 13” laptop that was designed from the ground up with repair and modularity in mind. _You_ might not remember, but _we’ve_ been anxiously waiting 4 long months for this thing, and it’s finally here: the framework laptop. Before we tear it down though, let’s rewind, because the Framework Laptop is _so_ DIY friendly that you have the option to build the thing yourself.

[Sony A1 Complete Disassembly And Teardown - Kolari Vision 2022](https://kolarivision.com/sony-a1-complete-disassembly-and-teardown/)

Interesting to see the evidence of product improvements over previous versions:

Weather sealing: 

>Looking closely at the edge, we can see that they’ve added some weather sealing material under the seam.

Thermal performance:

> The metal thermal frame is the first thing that catches my eye. We’ve seen similar plates in the A7RII, but these were absent in the III, RIII, & RIV. Now they added an updated version for the A1, as well as the A7SIII. It’s no surprise that they needed to add more heat distribution considering performance those cameras are offering 4k 120FPS and 8k.

And: 

>Beneath the board, is another layer of metal heat sink. This one feels more condensed than what we’ve seen in other models. On the A7III and A7RIV, it stretched over to the battery compartment, and the A7III’s heatsink had ‘feet’ that extended down to attach to the front. Also, with the A7RIV they went with a more skeletal design, leaving gaps in the plate to cut down on the weight. Here, however, they have prioritized keeping it cool to keep up with the performance demands. They’ve included a layer of heat pads in the middle.

Design for repair: 

>Another nice little update is the wings on the NFC flex cable that hook into the heat plate and hold it in place. It’s always hard trying to wrangle the different ribbons in place around the board, so this will make reassembly a bit easier.



# Manufacturing Books

[Rob Thompson: Manufacturing Processes for Design Professionals](https://www.amazon.co.uk/dp/0500513759/ref=pe_27063361_487055811_TE_dp_1)
General text on a wide variety of manufacturing processes


[Rob Thompson:  The Materials Sourcebook for Design Professionals](https://www.amazon.co.uk/Materials-Sourcebook-Design-Professionals/dp/0500518548/ref=sr_1_1?crid=20MGBAWWJURYW&keywords=materials+sourcebook&qid=1645186829&s=books&sprefix=materials+sourcebook%2Cstripbooks%2C47&sr=1-1)
Similar, but more of a focus on material selection


[Andrew Bunnie Huang: The Hardware Hacker](https://www.amazon.co.uk/Hardware-Hacker-Andrew-Huang/dp/159327758X/ref=sr_1_3?crid=1KNWZ4V71JEC2&dchild=1&keywords=Bunny+Huang&qid=1635419902&s=books&sprefix=bunny+huang%2Cstripbooks%2C96&sr=1-3)
Great insight on manufacturing electronics products in China

## DFMA

[K. Swift, Manufacturing Process Selection Handbook](https://www.amazon.co.uk/Manufacturing-Process-Selection-Handbook-Swift/dp/0080993605/ref=sr_1_1?crid=2YQSFGPGQQ24E&keywords=manufacturing+process+selection&qid=1646997532&s=books&sprefix=manufacturing+process+selection%2Cstripbooks%2C72&sr=1-1)
Heavy focus on DMA principles and cost calculations

## Teardowns

[Bryan Bergeron:  Teardowns: Learn How Electronics Work by Taking Them Apart](https://www.amazon.co.uk/Teardowns-Learn-Electronics-Taking-Apart/dp/0071713344)
On teardowns of electronic devices

[Todd McLellan: Things Come Apart 2.0: A Teardown Manual for Modern Living](https://www.amazon.co.uk/Things-Come-Apart-2-0-Teardown/dp/0500294879)
More visual teardowns
