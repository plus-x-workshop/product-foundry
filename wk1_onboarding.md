---
layout: page
title: Week 1 - Onboarding
permalink: /week1/

---

# Onboarding



## Objectives

- Programme and building essentials
- Find out more about your projects
- Introduce some design tools
- Experience working through a design process on a fictional product
- Use a brief to investigate your own project


## Resource

### Who's delivering 

- AS
- JT
- KI

### Materials and tools

#### Prompt cards for ideation session

- Input
- Output
- People / users
- Time of day/event
- Activity/behaviour
- Furniture/Appliance/Room
- Reaction/effect
- Surprise / Fun

#### Craft Materials and tools

- Card
- Cardboard boxes
- Hot glue guns and glue
- Cable ties
- Gaffer tape
- Safe craft knives
- Elastic bands
- Littlebits
- etc.


## Agenda

| Time    | Lead  | Activity                                  |
| ------- | ----- | ----------------------------------------- |
| 10.00am | AS    | Introduction to the programme and space   |
| 10.15am | All   | Group introductions                       |
| 10.30am | KI    | Tour of the workshop                      |
| 10.45am | AS    | Introduction to Design Innovation Process |
| 11.00am | JT/AS | Activity Pt. 1 (intro, ideation, making)  |
| 12.00pm | All   | Introducing Design Documents              |
| 12.30pm | All   | Lunch                                     |
| 1.00pm  | JT/AS | Activity Pt. 2 (demo briefs and own innovation briefs) |
| 2.00pm  | All   | Finish core session                       |

TODO: Profile shots


## Introduction


### Plus X

#### People

- Andrew Sleigh, Workshop Manager 
- Katie Isard, Workshop Technician
- James Tooze, Course Leader, Product Design, University of Brighton
- Lisa Ellwood, Programme Delivery Manager
- Marta Jani, Programme Delivery Co-ordinator

- Zach and Ashleigh - photography and case studies
- Consultant experts
- Plus X members and workshop users, and wider network

#### The building and workshop

- Access and Kisi
- Project storage
- Membership
- FOH - Fire and Safety


### Participants

- Introductions and innovation projects


### Product Foundry


#### BRITE
- Brighton Research Innovation Technology Exchange
- The project is receiving up to £5.5m of funding from the European Regional Development Fund as part of the European Structural and Investment Funds Growth Programme 2014-2020.
- Partners: Plus X and University of Brighton
- Other programmes: https://www.briteinnovation.co.uk/brite-programmes

#### Schedule

- Tuesdays: 10am – 2pm, except final week
- 30 minutes for lunch, provided
- Final session (show-and-tell, and next steps): 2pm – 5pm

#### Record-keeping
- Each have a register, ask you to sign in to each session, and agree to outcomes
- Anonymous feedback

#### Confidentiality
- You own your IP
- You must keep others work confidential
- This is a shared space

#### Marketing
Photography (headshots/WiP)
Case studies


#### What we're aiming to do

- Introduce you to some tools that you can use to help you move your idea forward:
	- Physical tools like 3D printers
	- Design process tools like user-testing methods
- Hands-on experience: designing, making, testing
- Give you an overview so you have a sense of the whole space, and know where you need to fill in more detail
- Help you become more comfortable with the tools so you can develop more expertise 
- Give you the language so you know what to search for (books, Google, help forums, YouTube)

#### What we're not aiming to do

- Teach you everything!
- Fit entire product prototyping process into 7 weeks
- Apply every skill or activity to your current product: give you skills you can use later

Ask about one-to-one sessions or other access.




#### What can you do in a workshop

- Prototyping vs manufacturing
- Typical materials
- How long it takes to make something



#### What you need: Technology and tools		
- Computer
- Software, optional:
	- 2D CAD/vector drawing: Illustrator, [Inkscape](https://inkscape.org) (free)
	- 3D CAD: Fusion360 ([free personal licence](https://www.autodesk.co.uk/products/fusion-360/personal), or ask me for a temporary fablab licence)
	- 3D printers: PrusaSlicer, Preform (for resin printers)


<!-- 

### Workshop introduction and tour

// Do this through materials/tool inventory in second week



Review available tools and working practices

-   Workshop tour
-   Basic Safety
-   Materials and tools spectrum

%%TODO - Katie to fill out plan%%
 -->

## BREAK

15 mins comfort break

## Design Innovation Process

### Innovation and product development problems

- Where do I start?
- How do I decide between alternatives?
- How do I come up with better ideas?
- How do I know if this is going to work?
- Can it be done?
- What do I Google for?
- Who can help me?


### Design Tools

- Birds-eye design processes, e.g Double diamond
- Methodologies: user-led design, generative design, making it up, benign dictatorship
- Design specification (briefs and product design specification (PDS))
- Designing, making, testing prototypes

Different making activities can be applied at different times in the process. Sometimes at multiple different stages.


### Design Process

There are paths we can follow and tools we can use to make sense of new product development and innovation.



It feels like this:

![](assets/squiggle-labels-outline.png)

From: https://thedesignsquiggle.com

But with reflection, and by applying tools, we can give it some structure

![](assets/Double-Diamond-Model-2019-crop.png)

Design Council's Double Diamond
Idealised diagram of a design process


https://www.designcouncil.org.uk/news-opinion/what-framework-innovation-design-councils-evolved-double-diamond


Key features:
- A rhythm of opening and closing - diverging and converging
- Works on many timescales (over 15 minutes, or 15 months)
- Not linear, nor as clean as this diagram suggests



### Applying tools


This tool has been adapted by many people.
This one by James at University of Brighton.

Focusing on elements that happen before or after what we think of as the design process
 - how do you get to the starting point
 - how are values reflected throughout the process

Giving names to activities that make sense in that context - everyone does this differently


We can use key tools throughout the process:
- Descriptions: briefs and specifications
- Designs: visual or physical representations (models, photos, renders)
- Artefacts: test, and testable prototypes
- Narratives: mission statement, pitch, plan of action


![](assets/brighton-pd-briefs-spec.png)

From: https://fixperts.brightonproduct.design/design-projects-and-process/design-process


---



## Activity: Starting, ideation and briefs

- We're going to go through a design process. 
- The first segment of the double diamond:
  - We have a starting point (an ambition, some values and a little research)
  - We want to come up with some ideas (diverge)
  - Then, by testing those ideas out, we can define our challenge better, and converge on a brief

### Introduction (15 mins)

<!-- 

Update for Joy-us

- Present fictional business (and values): STARTL – the internet of surprising things
- 'Research' we’ve done – 27% of participants wanted more fun and surprise in their home life.
- Ambition for this project – establish the market in fun & surprising IOT / MVSP
- Values: Our products are not useless, or just for fun. They are meant to be useful and fulfil a need. Bring happiness, not harm.

Some inspiration: 
- Dominic Wilcox
- [Japanese un-useless inventions: Chindogu](https://www.tofugu.com/japan/chindogu-japanese-inventions/) 
- @unnecessaryinventions on Instagram -->


### Group Ideation 

- Task: Considering our research, ambition and values, come up with some ideas for joyful home IoT products
- Existing smart home tech:
  - Assistants
  - Smart devices
  - Personal computers
  - Network


- Think about: 
  - the people in the home (families, housemates, visitors)
  - activities, and key events or times of day
  - what can we tell about the person, what they're doing, how they feel, or the state of the environment
  - what kind of effects we could produce, and how



<!-- Teams focus on different (meaningful) ideas:
- Low mood
- Help in a cooking/entertaining context
- Behaviour change/better habits
- Intergenerational harmony
- Crunch points in the household

Using prompt cards:
- to get an idea of a place to start
- to refine an existing idea, or swap out one element for another
- to add new ideas -->

In teams of 3-4, write down or sketch as many ideas as you can
Time: 15 mins



Pick your favourite and make a demo using the fabric of the space, craft materials, and/or play. Consider:
- Does it reflect our values?
- Is it a physical product?
- What does it need to work?
- What's the biggest question you'd need to answer
- What do you need to test first
- Which part are you wedded to?
- Which elements could be swapped out?
- How could it be improved?
- What untested assumptions are implied?
- What prerequisites hadn't you thought of?

Time: 15 mins

Every team demos their selected idea to the group
Time: 15 mins


Then – we're going to write briefs for these products
Converge towards a project brief

Then apply to your own project


---




### Introducing Design Documents

We use documentation to help us:
- Articulate problems and needs
- Share our intent with others
- Refer back to, to check we're on course
- Prioritise competing requirements

Here, as a tool to prompt a dialogue about the idea
Can you achieve your aspiration?



#### Brief

See Rodgers/Milton, Product Design (p, 65)

"A statement of intent"

- What is the problem you're trying to solve / what gap are you trying to close
- Not: what is the solution, or fully specified set of requirements

An unusual example:

Citroën 2CV (pronounced “deux chevaux,” or “two horses” in English). Original brief by Pierre-Jules Boulanger, chairman of the French carmaker Citroën:

> A low priced rugged "umbrella on 4 wheels" that would enable two peasants to drive 100kg of farm goods to market at 60km/h, in clogs, and across muddy unpaved roads if necessary

- Interesting that he didn't state the solution: e.g. a harness and cart for 2 horses. 
- He was competing with a different solution: horses

Typically might cover:

- Marketing
	- Who is it for?
	- Wish list of features or customer 'must haves'
	- Benefits over competitors (USPs)
	- Position in the market
	- Relationship to other products in brand or range

- Technical
	- Functions (as opposed to 'features')
	- Initial specification of performance, cost, intended manufacture
	- Standards and compliance needs
	- Production volume

- Sales
	- Intended margin
	- Channels, markets
	- Distribution
	- Social, environmental impacts


### PDS (Product Design Specification)

#### What is it?

"A description of the desired solution to a problem ... by a set of requirements" (See https://www.designsociety.org/download-publication/29441/the_role_of_a_specification_in_the_design_process_a_case_study/ (PDF))

But - doesn't predict the outcome: It "defines the task by listing all the conditions the product will have to meet ... defining something that does not yet exist" (Rodgers/Milton, Product Design, p. 72)

Dynamic - as the design develops the PDS will change. You will discover things that make you re-prioritise needs, or resolve conflicting requirements.

Typical Elements, see Rodgers/Milton, Product Design, p. 74
 
<!-- 1. Performance
1. Environment
2. Maintenance
3. Target Product Cost
4. Competition
5. Packing
6. Shipping/Transport
7. Quantity
8.  Manufacturing Location, Expertise or Processes
9.  Size
10. Weight
11. Aesthetics
12. Materials
13. Product Life Span
14. Standards/Specifications
15. Ergonomics
16. Target Customer
17. Quality and Reliability
18. Shelf Life Storage
19. Timescale
20. Testing
21. Safety
22. Company Constraints
23. Market Constraints
24. Patents
25. Political/Social Implications
26. Legal
27. Installation
28. Documentation
29. Disposal -->



---


### Lunch

- 30 minute break, work through rest.



## Activity: STARTL briefs

Can we agree a set of briefs for our STARTL products?

Q - one brief, or one per product? e.g. 

- Who is it for?
- Wish list of features or customer 'must haves'
- Relationship to other products in brand or range
- What should it cost
- Standards and compliance needs
- Channels, markets
- Distribution
- Social, environmental impacts



## Activity: Your own innovation brief


In pairs: write down initial brief ideas for your innovation.

Develop these over the course of today, and future sessions, and use as reference for one-one consultancy or extra sessions.




## Next steps

How can you develop these ideas further?


### Future sessions

How do I build a better model? (more robust, cleaner, more realistic, functional)

Week 2: Digital Fabrication 101
- Learn how to use the tools in this space, starting with the fastest, most direct: laser cutter

How do I know if people will like, understand or be able to use it?

Week 3: Testing with People
- Learn about test design and making testable prototypes

How can I make something to inspire others (investors, or for advertising)?
How can I explore the form it should take – what it should look like?

Week 4: 'Looks like' Prototypes 
- Make 3D printed models, and look at computer aided design

How do I know if it can be made? 
What materials should it be made from?
How much will it cost to make?

Week 5: DFMA
- Learn about manufacturing and assembly within a design process, and tear down other products to see how they're made.

How can I share my ideas with others?

Week 6: Presentation
- How to make hi-res 3D models
- How to visualise products in CAD
- How to photograph products effectively

