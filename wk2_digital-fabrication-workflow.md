---
layout: page
title: Week 2 - Digital Fabrication Workflow
permalink: /week2/
---

# Digital Fabrication Workflow


## Objectives

-   Introduction to digital fabrication tools and materials
-   Introduction to a basic digital fabrication workflow
    -   How tools/processes constrain/enable design decisions 
    -   Software toolchain: CAD, CAM, machine control
    -   Machines and physical outputs: safety, mistakes, clean-up, redesign, optimisation
-   Consider workflows and tools in relation to your own projects


## Resources

### Who's delivering 

* Andrew
* Katie
* Emmanuel

### Materials and tools

%%TODO%%

Materials for laser cutter:
- Laser-able card
- Sheet materials (MDF, ply, card)
- Single ply card cut to laser bed size

<!-- 
Typical materials for other tools (demo):
- Plywood
- MDF
- Leather, felt
- Aluminium
- Delrin
- Experimental materials: PLA sheets, seaweed, ZWC plastics
- Filament (PLA, TPU, PETG)
- Resin
- Silicon/Resin/Jewellers wax
- PCB FR1 + components
- Arduinos, Pis and kits 
- -->



## Agenda

| Time    | Lead | Activity                           |
| ------- | ---- | ---------------------------------- |
| 10.00am | AS   | Intro talk and review              |
| 10.15am | AS   | Introduction to dig.fab. workflows |
| 11.00am | All  | Laser activity (1st iteration)     |
| 12.30pm | All  | Lunch                              |
| 1.00pm  | All  | Review and iterate                 |
| 2.00pm  | All  | Finish core session                |



## Prep/Before session

Create sample 3D models in 2D materials:

- Press fit construction
- Living hinges
- Finger joints 


## Digital Fabrication

### What Is It (Good For)?

* 'Bits to atoms' (as compared to traditional workshop tools)
* Accessible mechanised production (accurate, repeatable, less craft skill required)
* Rapid prototyping
* Small batch production, customisation and distributed manufacturing

### What Can You Make

Examples: <https://fablabbrighton.github.io/digital-fabrication-module/course-notes-lm225-2020/examples>

### What's in a Digital Fabrication Workshop

* Machines
* Materials
* Software
* Culture


#### Activity: Machines and Materials Inventory

##### Machines

- What do we have?
- Arrange on a spectrum: slow - fast

- What other trade-offs or considerations are there?
	- e.g. waste, danger, reliability, accessibility, scalability, similarity to manufacture


##### Materials


Not materials library
Careful of metal bin

- What do we have?
- Arrange on a spectrum: 
	-  Prototyping <-> manufacturing
- Consider:
	* Cost
	* Sustainability
	* Functional characteristics
	* Quality of finish
	* Availability
	* Ease of use



#### Machines

Types:
* Cutting tools
* Printing tools
* Milling tools
* Assembly tools
* Finishing tools

What are they good for?


#### Materials

* Prototyping vs manufacturing materials
	* Cost
	* Functional characteristics
	* Ease of use
	* Quality of finish
	* Availability
* Materials that can be cut, melted, milled, assembled
* Sustainable materials


#### Software

* Design and machine control
* Open source and proprietary (e.g. Fab Modules, Inkscape, Blender)


#### Culture

* Open and collaborative
* Standing on the shoulders of giants


## The Laser Cutter

### What it does

* Laser beam (very high intensity light) is fired onto a mirror.
* The mirror and lens travels around on a gantry (x/y) and directs the focused laser beam (about 0.2 mm wide) onto the material
* It follows the paths you create in your vector artwork

### What you can do 
* Raster engraving
* Vector cutting

### Materials

* (Mostly) flat sheets, but also, guitars, glasses, etc!
* Cardboard - Use old boxes, very forgiving for joints
* [Plywood](https://www.towerhobbies.com/cgi-bin/wti0091p?&P=ML&C=RCC&V=RMX&D=Revell-Wood---Plywood) - Real thickness != stated thickness - use the callipers.
* [PMMA/acrylic/plexiglass/perspex/lucite](https://www.mcmaster.com/#acrylic/=1bgkrkx)   [glue](https://www.mcmaster.com/#standard-acrylic-glue/=1bgkitd)   [bend](https://makezine.com/2013/02/06/workshop-wednesday-heat-bending-acrylic-enclosures)   
* [POM/delrin/acetal](http://www.mcmaster.com/#acetal-homopolymer-sheets) - more elastic than acrylic

### Applications

- Packaging prototypes (box nets/cardboard engineering)
- Custom housings for electronics, etc. (Tidey)
- Textile and fabrics cutting / assembly (soft fabrication)
- Signage, branding, image engraving
- Decorative effects (Burnt Axe)
- Complex cutouts (Wyrde Woods)


### Digital Fabrication Workflow

* Design (CAD)
* Prepare files for the machine (CAM)
* Make
* (Finish, assemble)
* Test and repeat


### Designing to the Machine

All tools have things they're good or bad at e.g. with Laser:

* Pros: Fast cutting 2D sheet materials
* Cons: Can only make 2D assemblies, limited materials, laser beam does have a width ("kerf")

* Design to the tool
* Allowances for kerf
* Choosing different tools for parts of the job
* Assembling different components
* Useful non-digital tools: glue and sandpaper!


### "Ready, Fire, Aim"

Make something as quickly as possible, then improve it, e.g. with laser:

* Kerf, tolerances and fit, 'dimensional accuracy'
* Cut quality, burning and surface finish
* Will your assembly work?


## Activity 

Objectives
- Everyone goes through whole workflow
- Get from zero to cutting ASAP
- Focus on machine, not CAD (unless people know it)
- Option to more expert people to do their own thing (with help?)
- Everyone safely trained on the machine


<!-- Issues..
- Finish at different times, how to start next activity?
- Go through our workflow...
- How will laser inductions work - TODO -->


Task: Laser-cut an assembly from corrugated card. Assemble and refine, or produce your own assembly, designing to the tool


Choose an existing model and set parameters (or - your own assembly)
Must fit onto sheet (A2: 594mm x 420mm)
Download vector file (SVG, DXF)
Load into laser software
Cut from corrugated card
Iterate: Improve fit, different material, more complex assembly 



#### Materials

- Single ply corrugated card
- TODO: thin card for box nets
- Plywood for second iteration

### Design (CAD)


<!-- * #### Designing to the tool
* 
* Q - what are the limitations of the tool?
* 
* 
* To consider:
+ The laser has a thickness – how will this affect the size and fit of your parts?
+ Your material is being vaporised or burned away. How could this affect the finished result? How could you account for it?
+ The laser cutter works most easily with flat (mostly rigid) sheet materials. If this is a limitation for you, how can you work around it?
+ You can only cut some materials. If this is a limitation for you, how can you work around it?
* 
*  -->
<!-- 
#### Pre-made models

Start with some pre-made options. (In order of complexity)


Criteria: 
Ideally at least 2 parts that fit together: e.g.
- Box assembly
- A body with a lid
- Gears or puzzles that fit together
- A mechanism


- Pick one
- Generate SVG file from online tool
- Prepare for cutting
 -->

##### Assembled boxes

Simple box:
https://www.festi.info/boxes.py/ABox?language=en

Post-it note box:
https://www.festi.info/boxes.py/NotesHolder?language=en

Box with rounded hinged lid: 
https://www.festi.info/boxes.py/FlexBox2?language=en

Box with sliding flexible lid
https://www.festi.info/boxes.py/ShutterBox?language=en

Many more boxes: 
https://www.festi.info/boxes.py/?language=en

##### Folded boxes

Folding card box net
https://www.templatemaker.nl/en/

##### Gears

http://hessmer.org/gears/InvoluteSpurGearBuilder.html?circularPitch=8&pressureAngle=20&clearance=0.05&backlash=0.05&profileShift=0&gear1ToothCount=30&gear1CenterHoleDiamater=4&gear2ToothCount=8&gear2CenterHoleDiamater=4&showOption=3

[Good example here](http://www.hessmer.org/blog/2020/09/27/online-involute-spur-gear-builder-v2/) of the complexity you encounter trying to make something real:

> Version 2 creates higher quality output without ragged edges at higher speed. Like before, undercuts as needed for small tooth counts are correctly handled; clearance, backlash, and profile shift are fully supported.

##### Maze
https://adashrod.github.io/LaserCutMazes/designer

##### Jigsaw
https://gists.rawgit.com/Draradech/35d36347312ca6d0887aa7d55f366e30/raw/b04cf9cd63a59571910cb226226ce2b3ed46af46/jigsaw.html


(Source: https://makerdesignlab.com/tutorials-tips/online-file-generators-for-laser-cutting/)


- Demonstrate these


### Laser gotchas

Order: score/engrave, then cut
If paper it may curl up and laser defocuses
Line colours


Units of measurement
Different for engrave and cut!
Minimum and maximum values - e.g. 10% power cut does nothing, 11% does!




### Prepare files for the machine (CAM)

Summary:
- Cleanup and finalise artwork in 2D vector editor
- 'Print' to the laser cutter software (JobControl)
- Arrange artwork on the bed, and confirm cut settings

#### Machine control for lasers

Laser cutters interpret 2D vector artwork
- Lines: follow with laser to cut through material
- Solid colours: move laser back and forwards (like a paint roller) to engrave or mark material
- **Beware**: a thick line looks to the laser like a solid colour! (use very thin lines for anything you want to cut - e.g. 0.01 mm)

Very limited range of settings you can control (from the Trotec manual):

![[laser-settings.png]]
 
> **Power**
> Percentage of the maximum laser power.
> The engraving depth depends basically on the laser power and speed set. Increased power as well as decreased speed results in deeper engraving or cutting. Adjustment range: 0-100% (100% is equivalent to the maximum power of the laser).
> **Speed**
> Percentage of the maximum speed.
> Important: Cutting speed percentage relates to maximum engraving speed. This means that if your laser machine has a maximum engraving speed of 355 cm per second, setting of 1 % means 3.55 cm per second cutting speed.
> **PPI**
> Pulses per inch (laser pulses per inch).
> This setting determines the number of laser pulses per inch emitted by the laser. 
> Adjustment range: 500 - 1000 PPI
> To obtain a good result, the PPI value should be usually larger than or at least the same as the dpi setting of the printer driver (Process options – Resolution), e.g. a minimum value of 500 PPI should be selected for a resolution of 500 dpi.
> Unlike the resolution in dpi, increase in PPI does not have an effect on the engraving time.
> **Hz**
> Frequency of laser pulses during cutting. This setting determines the number of laser pulses per second emitted by the laser. 


Use colours to choose different laser settings. Defaults:
- Red: cut
- Black: engrave

Some useful options you could use other colours for:
- Cut, but with a very low power – has the effect of scoring a line, e.g. in felt, leather or card, or marking a pattern in plywood or acrylic
- Don't cut ("skip" in the software), just use as reference (e.g. instructions or guides in the artwork)
- Different engraving power settings

All these files will require cleanup

- Correct line colours
- Correct line widths
- Remove extra content (labels etc.)
- Check right size
- Check for weird bugs! (e.g. artwork not really paths, something off the artboard, )

Finish setting up job in laser cutter software (cover in machine induction)


### Make

When ready, go to one of the machines, get induction.

Make test cut in waste material, then if all OK, use real stock

Q - (Groups?)

Safety considerations: lasers are simple and fast, but also the most likely tool to catch fire.

### (Finish, assemble)

Minimal clean-up likely
Assemble your product

### Test and repeat

Did it work?

- How good is the quality of the cut?
- How good is the surface finish for engraved parts?
- Are there burned areas in the cuts or on the surface (could you use a lower power, or protect the surface?)
- Did it fail to cut through the material?
- If the scrap test failed, can you adjust the artwork?
- Is the fit good? Could you adjust the kerf settings to compensate?


Try again

- Make something more complex
- Improve the quality of the cut





## Alternative Activities

If you:
- Know how to use a laser cutter
- Don't need this kind of construction/materials

Options
- Different materials: e.g. felt
- Materials test: e.g. experimenting with cut settings for novel materials such as vegan leather or bioplastics
- A different machine: e.g. vinyl cutter, flatbed CNC, Shaper.


<!-- TODO -->



## Further reading/watching

Tutorial videos: 

* <https://www.linkedin.com/learning/illustrator-2020-essential-training> 
* <https://www.linkedin.com/learning/laser-cutting-design-for-fabrication>