# Prototyping





## Technology Exploration

http://dominicwilcox.com/?portfolio=the-reinvention-of-normal

![[wilcox 1.png]]

McGuyver Reading light
https://www.instagram.com/reel/CYznRRTg6zC/?utm_medium=share_sheet
Laura Kampf exploring an idea through play with materials


## Cardboard Modelling

https://www.m3design.com/mockups-speed-up-development/


https://www.ruturaj.design/cassette.html

http://www.judepullen.com/designmodelling/design-modelling-workshop-prototype-in-cardfoamplastic/



## Reading/watching list

- Tinkering Studio
- Lifelong Kindergarten
- Tim Hunkin
- Simone Girtz
- Dominic Wilcox

---

# Reference/research/notes

      

**Accessible physical computing electronics**

[Home, Chibitronics](https://chibitronics.com/)

[Community Projects](https://www.bareconductive.com/blogs/community/tagged/design)

[Kits - Seeed Studio Electronics](https://www.seeedstudio.com/category/Kits-for-Grove-c-28.html?cat=917)

[Get Started with littleBits STEAM+ Coding Kit](https://classroom.littlebits.com/kits/steam+coding)

[littleBits | Shop littleBits: STEM Kits & More | Sphero](https://sphero.com/collections/all/family_littlebits)

  

Also see:

Tinkering studio workshop ideas (circuit blocks)

  

Bare conductive workshops

  

Chibitronics book

  

  

**Littlebits**

  

[https://classroom.littlebits.com/lessons/magic-of-invention-workshop-guide](https://classroom.littlebits.com/lessons/magic-of-invention-workshop-guide)

In your small teams, you will use littleBits and other materials to create an invention inspired by Harry Potter!! You can cast wireless spells, recreate your favorite scene, build a magical animatronic creature, forge an enchanted tool, and more.