# Materials Links

[Thinking Materials]() (http://www.thinkingmaterials.net/index.php/cards/)

[PDF Download](http://www.thinkingmaterials.net/Thinking_Materials_Cards.pdf)

>The Thinking Materials design tool identifies creative product opportunities that use indigenous materials and crafts as an alternative to more conventional manufacturing processes. It demonstrates the potential for a supply chain in which an emerging economy is always the country of origin (COO) for the manufacture of a product and/or growth/production of materials.



[Ansys Granta EduPack](https://www.ansys.com/en-gb/products/materials/granta-edupack)

>Ansys Granta EduPack—formerly CES EduPack—is a unique set of teaching resources that help academics enhance courses related to materials across engineering, design, science and sustainable development.

[Materiom](https://materiom.org)

>Materiom provides open source recipes and data on materials made from abundant sources of natural ingredients, like agricultural waste.