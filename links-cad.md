# Links and books for CAD and 3D modelling


## Books


### Fusion 360 for absolute beginners


See: UOB Module on Fusion

Fusion book


## Videos

[Fusion 360 For Beginners - Recorded Webinar](https://www.youtube.com/watch?v=VbSkwvZyU_0)

This session is to help you learn the basics in Fusion 360. This is intended for those out there who are completely new users of Fusion 360 and 3D design. After an introduction to the user interface, time will be spent learning how to navigate, model with primitives, model from sketches, and assembly design/joints will be covered.


Blender Doughnut tutorial