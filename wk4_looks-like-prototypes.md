---
layout: page
title: Week 4 - 'Looks Like' Prototypes
permalink: /week4/
---

# 'Looks Like' Prototypes

## Outcomes

- Understand the role of 'looks like' prototypes
- Understand how tools/processes constrain/enable 3D design decisions 
- Hands-on experience with 3D printing toolchain: CAD, CAM, optimisation
- Hands-on experience with machines and physical outputs: safety, mistakes, cleanup, redesign, optimisation
- Able to make hi-fi appearance prototypes to communicate product vision to investors
- Developed innovation by working out of finish and form details


## Resource



- Andrew
- Katie
- Emmanuel

<!-- Download and install
- Fusion (I can assign licences?) TBC
- PrusaSlicer on all lab computers
 -->


## Agenda


| Time    | Lead | Activity                          |
| ------- | ---- | --------------------------------- |
| 10.00am | AS   | Review last week                  |
| 10.15am | AS   | Intro to 'looks like' prototyping |
| 11.00am | All  | Intro to 3D printing              |
| 11.30am | All  | Activity: 3D fabrication          |
| 12.30pm | All  | Lunch and UoB talk                |
| 1.00pm  | All  | Activity: Continue 3D fabrication |
| 2.00pm  | All  | Finish core session               |



## Introduction

## Why make a 'looks like' model?

- Communication to non-specialists
- Inspiring stakeholders
- Living with something that you wear or hold
- Making it absolutely clear what something is supposed to look like (e.g. for a manufacturer)
- Highlight critical manufacturing puzzles to solve (or constraints to meet)

Although it often happens later in the process, this can be used as a divergent tool to explore multiple design ideas. Sometimes these are best explored with more detailed/refined models.

### Things you can test or share

- Overall form, or variants (silhouette, exterior form)
- Finish, feel, colours
- A specific function or sub-system, eg. 
	- Screen UI
	- Handle, seat, strap
- Packaging, branding


## What it doesn't do

- Function, typically
- Manufacturing materials (depending on which stage you're at)


## Discussion: What could you make?

- What would you like to communicate or test visually?
- What part of a product?
- Is it packaging or distribution or point of sale?
- How could we make it?


## Materials and processes to make them 

- Hand-carved foam or clay
- CAD render
- 3D print (FDM, resin, SLS)
- CNC mill
- Cast (from CNC mould)
- Assembled from other materials: textiles, metals, card, wood, etc)
- Off-the-shelf parts



## 3D printing

We're going to focus on the most versatile, accessible method: 3D printing

* 'Additive manufacturing', ie. low waste (lego, not sculpture)
* The most direct form of 'bits to atoms' making
* Processes (we have here): 
-  [FDM technology](https://www.prusa3d.com/category/original-prusa-i3-mk3s/)
	- Layers of melted plastic filament
	- Fast, low-res,  cheap, easy
	- Materials: **PLA**, ABS, TPU, Nylon, PETG
- [SLA/stereolithography technology](https://formlabs.com/uk/3d-printers/form-3/)
	- resin cured selectively by UV laser in layers
	- Slower, higher-res, stronger parts; messy, fiddly cleanup
	- [Many functional materials](https://formlabs.com/uk/materials/)


### Benefits and opportunities

* **Affordable, hi-quality one-offs (no tooling)**
* Accessible: Little craft skill required, cheap machines and materials 
* Rough models can be finished with some effort to a high standard
* Make custom fixings to join dissimilar parts (amplify existing capabilities)
* Personalised designs and customisation
* Programmatic/parametric customisation


### Making things other than your product

- Jigs
- Moulds (Jazmine's latex form, [Formlabs injection molds](https://formlabs.com/uk/applications/injection-molding/))
- Components (e.g. hard plastic parts for a textile assembly)
- Scale models 


### Scaling up / next steps

- SLS Printing (e.g. [Shapeways, inc TPU printing](https://www.shapeways.com/3d-print-material-technology/sls)) 
- Machining: https://www.protolabs.com/services/cnc-machining/cnc-milling/



### Workflow for 3D printing (FDM)

- Design (CAD)
- Prepare it for the machine ("slicing") (CAM)
- Set up the machine
- Print, post-process 
- Review results and iterate

As with DFMA, there is also DFP(rototyping). So we're going to run through whole process from after the design phase to help you understand what constraints and opportunities of these machines are.

### Constraints / Design rules

Activity: how can we get around these limitations

* Print volume: roughly 20cm-sided cube
* Always a tradeoff between print quality and time
* Print time can be slow for large models: e.g.  12-24 hours 
* Printing layer by layer: think about overhangs and support (consider rotating model)
* Aesthetic limitations: resolution of 1/5 mm; plastic finish
* Mechanical weakness: plastic material, printed in layers
* Material vulnerabilities (heat, moisture, light)


### Workarounds

- Split model into sections and assemble
- Use brims and support
- Assemble with off-the-shelf hardware: captive nuts, bearings, metal rods
- Combine with other processes: laser-cut panels, aluminium extrusion, 2x4s
- Finish with primer and spray paint

Fancy printer features
- Over or under-extrude
- Top-layer ironing
- Textured beds


### Common causes of print failure

- Poor bed adhesion
- Weird plastic mess: stringing, oozing, etc.
- Support fails
- Filament snaps
- Nozzle collides with model, result = spaghetti

### Design to the machine

- Flat base
- Limit overhangs
- Nice face down
- As close to a pyramid as possible
- Minimum feature size (FDM: 2mm, SLA: 0.2mm)
- Tolerances
- SLA: escape holes for uncured resin


Question: how would these be different in manufacturing? 


## Activity: Fab 3D models


### FDM Print existing model

For people new to 3D printing

#### Find a model

Download from a free source:
- https://www.printables.com/model
- https://www.thingiverse.com
- https://cults3d.com/en/collections

Search for something you like the look of and download STL file. Find something small and simple - the size of a Pritt Stick / Post-it pad

Or use one of the sample Prusa models:
- ![[assets/stl/3DBenchy.stl]]
- ![[assets/stl/3DHubs_Marvin.stl]]
- ![[assets/stl/Batman.stl]] (flat: very simple)
- ![[assets/stl/Buddy.stl]]
- ![[assets/stl/Gear_Bearing.stl]] (complex but has interesting assembly)
- ![[assets/stl/Treefrog.stl]] (interesting test of overhangs - will this print without supports?)
- ![[assets/stl/Whistle.stl]] (interesting support puzzle: how can you orient this so you don't need supports?)

#### Slice in PrusaSlicer

**Software**
- Download software: https://www.prusa3d.com/page/prusaslicer_424/
- Instructions: https://help.prusa3d.com/article/first-print-with-prusaslicer_1753

**Setup**

- Material: PLA+ /  Prusament PLA / Generic PLA
- Machine: Prusa i3 MK3S, 0.4 mm nozzle
- Target print time: 30 mins - 1 hour (reduce quality or scale until you can hit this target)

Some suggested settings to start with

**Plater tab**

- Simple mode in PrusaSlicer: "0.20 mm Speed"
- Supports: everywhere
- Infill: 20%
- Brim: Yes

**Print settings tab**

- Leave as is
- (Layer height: 0.2mm)

**Filament settings tab**

- Leave as is
- (Nozzle temp: 60°C)
- (Bed temp: 215°C)

**Slice and export**

- Press `Slice Now`
- Preview print: 
	- check print time and adjust settings if necessary
	- check supports and brim – will it print?
- Press `Export G-code`
- Save G Code to an SD card



#### Set up machine

- Load filament
- Load file onto SD card
- Start print
- Verify adhesion of first layer
- Go have lunch!

#### Review and iterate

- Did the print succeed?
- If it failed, how could you adjust the design or printer setup?
- How could the quality be improved?
- Are the dimensional accuracy or tolerances good enough?
- How could you finish the model?
  



### Resin-print existing model or your own design

If you have FDM experience and want to try resin printing


### Work on CAD/CAM for custom part

If there is a specific part you want to make and you have 3D printing experience


### Another process if relevant

Work with support from the team on a specific visual prototype
