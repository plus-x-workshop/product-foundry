---
layout: page
title: Week 6 - Presentation
permalink: /week6/
---

# Presentation



## Resources

### Who's delivering 

* Andrew
* Katie
- CAD consultant TBC
- Photography consultant TBC
- Rendering consultant TBC
- 3D printing consultant TBC


### Materials and tools

- Camera and product photo table and lights
- Book out photo studio
- Resin printers 



## Agenda

| Time    | Lead | Activity                                 |
| ------- | ---- | ---------------------------------------- |
| 10.00am | AS   | Intro talk and review                    |
| 10.15am | AS   | Introduction to presentation and sharing |
| 10.30am | All  | Split into groups per format             |
| 11.00am | All  | Asset production                         |
| 12.30pm | All  | Lunch                                    |
| 1.15pm  | TM   | Continue asset production                |
| 2.00pm  | All  | Finish core session                      |

## Outcomes

- Have considered needs of audiences when creating sharable representations
- Have hi-fi representations in appropriate formats that they can share
- Considered how to present product prototypes to potential partners, investors, collaborators, etc
- Developed proposition for communication
- Have integrated learning from other forms of prototyping into communication


## Why do we need to think about presentation

Sharing
Communicating
Selling


## Thinking about the audience


User-led approach to communication, just as with other design processes

Not what you want to say, but thinking about your audience
- Who are they
- When sharing your idea, what do you want other people to 
	- Think?
	- Do?
	- Say to others?

What can we make to help in that ?

e.g. 
- evidence a prototyping process
- Hi Q renders
- Hi FI models
- Robust DFMA review


### Propositions

TODO: 
- Narratives 
- Value propositions / ad libs
- Technical claims


## Discussion

- What do you want to share
- With whom
- To what end


## Activity

### Product photoshoot


### CAD renders


### Hi-fi 3D printing and finishing
