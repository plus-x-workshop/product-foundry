
## Testing


### [Testing with Humans: How to use experiments to drive faster, more informed decision making](https://www.amazon.co.uk/dp/0990800938)
Giff Constable, Frank Rimalovski


p. 13
Start by talking to customers and find out the critical factors - and what are the concerns they have when you talk about your idea.
Assuming you think you can meet these challenges, now you have some hypotheses to test. 

p. 16
Although it sounds counter-intuitive, you can fake the most critical part of your technology to test if your proposition actually makes sense - "wizard-of-oz prototyping"

Designing a test:
- A hypothesis
- A metric, with a pass/fail threshold
- A participant
- How many
- How will you get them
- A plan for the experiment (how to do, it how long)
- Are there other things we want to learn in this experiment?


Also see [Strategyzer's Test Card](https://www.strategyzer.com/blog/posts/2015/3/5/validate-your-ideas-with-the-test-card) (note this is based on 'Lean Startup' methodology)

![](assets/the-test-card.png)

p. 20
"_No plan survives first contact with the enemy_"

Your test will not go as you expect. 
You may not even be able to collect the data you expected to get
You users may be unimpressed with your amazing breakthrough. 
They might hold it upside down, or get excited about something you think irrelevant.

Tests are iterative, just like prototypes


p. 30

Prioritising assumptions and risks:

2 x 2 matrix: Impact and uncertainty - you want to test the risky assumptions (top right)

p. 35 
Test archteypes

- Landing page or response advertising
- Pre-selling
- Paper mockups (interface or data)
- Product prototype, pilot
- "Wizard of Oz" (hidden) or "Concierge" (open)



p.39 
Test design detail


### A hypothesis

- Experience: "Children aged 5-7 will enjoy playing with our talking teddy for at least 20 minutes"
- Function: "Our webcam-enabled bird feeder will survive a 2 hour rain shower"
- Value: "Construction site managers will pay £100/person/month for our PPE service"


### A metric, with a pass/fail threshold

Difficult to pick something that seems arbitrary. 
This is not science; you could do some modelling of outcomes, but likely still an educated guess, e.g. 

- "70% of children will continue playing with the toy for 20 minutes"
- "The device will remain fully operational after 2 hours in a vertical water shower environment"
- "10% of landing page visitors will sign up to our monthly plan"




### Participants (if you're testign with people)

Customers
Exclude edge cases?
Pick the most important segment?
Use whoever's easiest to get hold of
Consider ethics and vulnerable groups
How are yuo going to select the best, if you're openly recruiting

#### How many
Do it in phases

#### How will you get them
- Email list
- Research prime targets
- Personal network
- Approaching strangers in situ (street, office, hallway, park, etc.)
- Online ads
- Embedding experiment into existing product experience

### A plan for the experiment (how to do, it how long)

Consider ethics (and their experience) if working with people
How are you going to get your data
How are you going to manage any sleight of hand

### Are there other things we want to learn in this experiment?

How to collect that data


 
## Coming up with Experiment ideas
p. 53


Just like other idea generation techniques - e. g. Crazy 8s

As a team, do this, with prompts:
- What can we do in one hour, one day, one month
- What exiting tech and materials can we re-use?
- What are the boring or crazy-sounding tests

Each person shares their results without debate
Then dot voting
Choose the top results
Refine

## Looking at the results and making decisions

p. 59

Consider how truthy the experiment is likely to be
From https://giffconstable.com/2013/06/the-truth-curve/

![](assets/truth-curve.png)

Check your biases:

- Anchoring on one piece of information
- Confirmation bias
- Bandwagon effect
- Sunk cost fallacy



### Ideology and Business vs Product
(not in this book)

Testing methods are rooted in ideologies, rarely stated. Lots of business model type tests value 'product-market fit' as the ultimate goal. 

But that is an ideological position (if people want to buy it, then it is worth making), that might lead you to make all sorts of troublesome products: cigarettes; candy crush; hummers

Also need to be aware of the differnece between the business and the product or technology.
We're focused on products and technooogy here, but sales and pricing, or distribution channels might be critical success factors for you.

(And these can influence product design, so important to consider even here)





### [D.School Bootleg](https://dschool.stanford.edu/resources/design-thinking-bootleg)

Identify a Variable

> Rather than prototype a complete mock- up of a solution, it’s more productive to single out and test a specific variable. Identifying a variable not only saves time and money (you don’t need to create all facets of a complex solution), it gives you the opportunity to test multiple prototypes, each varying in the one property. This encourages the user to make nuanced comparisons between prototypes and choose one option over another.   
> 
> How to identify a variable 
> Prototype with a purpose. Based on user needs and insights, identify one variable of your concept to flesh out and test. Then build a few iterations. Keep prototypes as low resolution as possible. 
> 
> Remember, a prototype doesn’t have to be, or even look like the solution. You might want to know how heavy a device should be. Create prototypes of varied weight, without making them operable. You may want to find out if users prefer delivery versus pick up. Build boxes for each service, without filling them. By selecting one variable to test, you can bring resolution to that aspect of your concept and slide one step closer to a design solution.


Wizard of Oz Prototyping 

> A wizard of Oz prototype fakes functionality that you want to test with users, saving you the time and money of actually creating it. Wizard of Oz prototypes often refer to prototypes of digital systems, in which the user thinks the response is computer-driven, when in fact it’s human-controlled.
> 
> How to create a Wizard of Oz prototype: 
> 
> Determine what you want to test. Then figure out how to fake that functionality and still give users an authentic experience. You can combine existing tools (tablets, email systems, Powerpoint) with human intervention to create the illusion of functionality. 
> 
> The company Aardvark needed to test user reactions to their online interface for connecting people with questions to qualified people with answers. Instead of coding, they used an instant messaging system and team members behind the scenes to physically route questions and answers to the right people. 
> 
> Wizard of Oz prototypes can be extended beyond the digital realm, to physical prototypes. You could prototype a vending machine without building the mechanics behind it—using a person hidden inside to deliver purchases.


User testing     

> Testing with users is a fundamental part of human-centered design. You test with users to not only refine your solution, but to better understand the people you’re designing for. When you test prototypes, consider both what you can learn about your solution and what you can learn about your user—you can always use more empathy.     
> 
> How to test with users:  
> 
> Let your user experience the prototype.   
> Show don’t tell. Put your prototype in the user’s hands (or your user in the prototype) and give only the basic context they need to understand what to do.   
> 
> Have them talk through their experience.   
> Use prompts. “Tell me what you’re thinking as you do this.”  
> 
> Actively observe.   
> Don’t immediately “correct” your user. Watch how they use (and misuse) your prototype.   
> 
> Follow up with questions.   
> This is often the most valuable part. “Show me why this would (not) work for you.” “Can you tell me how this made you feel?” “Why?” Answer questions with questions. “Well, what do you think that button does?”
> 


# Teardowns

Product Design: Techniques in Reverse Engineering and Product Development
Otto and Wood
Chapter 6, p. 197 on

## Why

- Key step in reverse engineering (ie understanding how an existing product works)
- Help benchmark competitor performance
- Help understand how to solve common design/engineering problems
- Build your knowledge of how things are made


## What is it

"The process of taking apart a product to understand it, and how the company making it succeeds"

What choices were made – and how did these lead to success (or failure)


## Process

### Establish objectives: what do you want to learn? 

e. g. 
- How much does it cost to make?
- How is a particular problem solved?
- What are the systems that make up the product?
- Why are they meeting some needs and not others?
- Where does it sit in the market? 
- Where is it made? (e.g. low cost or high cost labour)
- How is it assembled? (highly automated, or large amount of manual labour)
- What materials are used?
- How could it be repaired?
- Is it using common (platform) parts?
- Who are their suppliers?
- What regulations might they be meeting? (e.g. food safe, or EM interference)
- How is the product packaged, distributed, retailed, installed?
- Are there new needs you hadn't considered?
- Are there functions that could be improved?


### List the design issues

Try to predict or hypothesise: 

What are the customer needs? 
In what priority?
What is the functionality of the product?

TODO - some of this seems to happen after teardown...

### Plan

What measurement equipment do you need? (multimeter, camera, calipers, flow meter, hardness tester, etc.)


### Disassembly

- Take pictures at each stage
- Take each sub assembly one at a time
- Save parts
- Measure or analyse parts


Try to avoid destructive disassembly until you have tested any product or assembly functions

### BOM

If doing a DFM cost analysis, list all parts with this info (if applicable)

- Part number/name
- Quantity
- Function
- Mass / Dimensions / Finish (e.g. for metal or plastic parts)
- Manufacturing process
- Supplier and their part number (e.g. for ICs)


### Determining functions of assemblies and components

Take one part or assembly out
- What function does this serve?
- Could it be simplified?
- Could the part count be reduced?
- Could it be made easier to assemble?
- Is the design over-constrained (e.g. two or more parts giving the same function)?

### Measurement

- Bias is a problem in any forensic investigation
- Having measurable performance metrics can help give you more objective data
- Important to measure as much as possible before destroying the product –       a hierarchical approach
- Metrics should be prioritised based on hypothetical PDS

### Reporting

- Disassembly plan
- Exploded view
- BOM (p. 236)
- Function structure (diagram p. 238)

p 250
Black box model of a product: main functions of the product, material and energy inputs and outputs, and how it interacts with users and the environment





## Toy Fire truck tear down

Overall assembly 
- 1 screw holding battery compartment closed - why not a flexure?
- 3 screws holding shell apart

PCB

- Tilt switch
- Blob IC
- Strain relief on power cables
- Wires bridge top and bottom halves - difficult to assemble. Looks like they're hand-soldered to board (ie cheap manual labour, not automated)
- Labelled UK/0602. Is this a PCB for the UK market? Could be as voice is English, with a British accent. Do other markets have other PCBs?
- Copyright VTech 2019: Looks like this is the brand: https://www.vtech.co.uk/brands/brand_page/toottootdrivers

Bottom half

- Marked as ABS plastic
- Complex piano key buttons, each pushes a leaf spring which makes contact with a pad on the PCB. Does this offer any useful functionality?
- Can't understand function of button at back of truck. Seems not to actuate anything...
- Splits into 2 plastic sub-assemblies
- Power switch (most commonly used interface component?) is sandwiched between the two. No glue. Is this a point of failure?
- Wheels are two parts of plastic with metal pin axle - no glue. Pin has one-way insertion. Affordable low-friction movement, with easy assembly.
- Captive nut has no corresponding bolt on other part of assembly? Is this a shared sub-assembly? (No - on reassembly it turns out this is the nut the battery cover bolt attaches to.)
- Piano keys held in place by metal pin - possibly held in flexure - again, no glue

