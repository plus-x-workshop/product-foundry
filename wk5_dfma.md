---
layout: page
title: Week 5 - Design for Manufacturing and Assembly
permalink: /week5/
---


# Design for Manufacturing and Assembly

## Resources

### Who's delivering 

- Andrew
- Katie
- Tom Meades @ Zero Waste Club
- Dave Lock - Cast Iron CAD

### Materials and tools

- Teardown toolkits
- Sample products for teardown 
- Tear down books / links



## Objectives

- Familiar with the principles of Design for Manfacture and Assembly (DFMA)
- Able to examine products to learn about DFMA techniques
- Able to consider DFMA in earlier design stages
- Understand impact of manufacturing constraints on product details (e.g. pricing)



## Agenda

| Time    | Lead | Activity                                    |
| ------- | ---- | ------------------------------------------- |
| 10.00am | AS   | Intro talk and review                       |
| 10.15am | AS   | Introduction to DFMA                        |
| 10.30am | TM/DL   | DFMA in practice    |
| 11.00am | All  | Product tear-downs                          |
| 12.00pm | All  | Lunch                                       |
| 12.30pm  | TM/DL   | Review of participants' DFMA considerations |
| 2.00pm  | All  | Finish core session                         |


TODO

Dave intro - see Apple note
Shift around this agenda.

## Context/Objectives

- Why do things look the way they do - design does not happen in the abstract. It's constrained and enabled by manufacturing
- Manufacturing is not just a thing that happens at the end
- Impact of materials and assembly on pricing
- Manufacturing can be a strategic decision (e.g. Apple parts re-use, [car platforms](https://en.wikipedia.org/wiki/Car_platform))
- We can learn about competitors design decisions by reverse-engineering their products


## Introduction to DFMA

See: [K. Swift, Manufacturing Process Selection Handbook](https://www.amazon.co.uk/Manufacturing-Process-Selection-Handbook-Swift/dp/0080993605/ref=sr_1_1?crid=2YQSFGPGQQ24E&keywords=manufacturing+process+selection&qid=1646997532&s=books&sprefix=manufacturing+process+selection%2Cstripbooks%2C72&sr=1-1)

%%TODO - condense notes%%

## DFMA in Practice

Dave Lock, CIC



Tom Meades, Zero Waste Club
- Zero Waste Club product design and manufacturing decisions
- Development of the Zero Waste Club business
- How manufacturing/assembly informs their design work now (technical/material/process choices, communication with factories, product pricing, redesign, and sustainability considerations)


## Activity: Product Teardowns





### Why tear down products

- Understand how similar or competitor products are made
- Help benchmark competitor performance
- Help understand how to solve common design/engineering problems
- Build your knowledge of how things are made

### What can you learn


- How was it made?
- How has it worn over time / why did it break?
- How were certain features or functions achieved?
- What materials or components were used?
- What's the BOM, and how much did it cost?
- How could it be repaired or recycled?
- How could it be improved, made cheaper or more robust?
- What choices were made – and how did these lead to success (or failure)


### Process
- Establish objectives: what do you want to learn? (See [[further-notes]])
- What do you need to uncover or measure?
- What tools (disassembly, measurement) do you need?
- Careful disassembly (save parts, take photos)
- Test functionality of removed assemblies or remaining parts


### Questions to ask

- What function does this serve?
- Could it be simplified?
- Could the part count be reduced?
- Could it be made easier to assemble?
- Is the design over-constrained (e.g. two or more parts giving the same function)?


### Costing / Making a BoM

If doing a DFM cost analysis, list all parts with this info (if applicable)

- Part number/name
- Quantity
- Function
- Mass / Dimensions / Finish (e.g. for metal or plastic parts)
- Manufacturing process
- Supplier and their part number (e.g. for ICs)


### Safety

PPE
- Nitrile Gloves
- Safety glasses

Electrical hazards
- Capacitors

Mechanical hazards
- Using force to prise casings apart
- Tools breaking

Material hazards
- Broken glass
- Sharp edges
- Glues and other chemicals


### Post-teardown review 

How would you make this product 20% cheaper
How would you change the manufacturing process to hit a different target user



## DFMA review of participants' projects

Tom will join us to talk to participants about their particular manufacturing questions




## Reading/watching list

[links-manufacturing](links-manufacturing.md)
