# Design process introduction



## Design Innovation Process

### Innovation and product development problems

- Where do I start?
- How do I decide between alternatives?
- How do I come up with better ideas?
- How do I know if this is going to work?
- Can it be done?
- What do I Google for?
- Who can help me?


### Design tools

- Birds-eye design processes, e.g Double diamond
- Methodologies: user-led design, generative design, making it up, benign dictatorship
- Design specification (briefs and product design specification (PDS))
- Designing, making, testing prototypes

Different making activities can be applied at different times in the process. Sometimes at multiple different stages.


### Design Process

There are paths we can follow and tools we can use to make sense of new product development and innovation.



It feels like this:

![](assets/squiggle-labels-outline.png)

From: https://thedesignsquiggle.com

But with reflection, and by applying tools, we can give it some structure

![](assets/Double-Diamond-Model-2019-crop.png)


https://www.designcouncil.org.uk/news-opinion/what-framework-innovation-design-councils-evolved-double-diamond


Key features:
- A rhythm of opening and closing - diverging and converging

- There is a narrative that drives us and we may share with others
- But remember it's never as linear as this – don't be afraid to go backwards


### Applying tools

Can be punctuated with key docs: briefs and specifications
We can make things at each stage to help us progress (designs and artefacts)

![](brighton-pd-briefs-spec.png)

From: https://fixperts.brightonproduct.design/design-projects-and-process/design-process


### Documents

#### Brief

See Rodgers/Milton, Product Design (p, 65)

"A statement of intent"

What is the problem you're trying to solve / what gap are you trying to close
Not: what is the solution
Not: fully specified set of requirements

Example:
Citreon 2CV (pronounced “deux chevaux,” or “two horses” in English). Original brief by Pierre-Jules Boulanger, chairman of the French carmaker Citroën:

> A low priced rugged "umbrella on 4 wheels" that would enable two peasants to drive 100kg of farm goods to market at 60km/h, in clogs, and across muddy unpaved roads if necessary

Interesting that he didn't state the solution: e.g. a harness and cart for 2 horses. 
He was competing with a different solution: horses


Marketing


Technical

Sales



### PDS

"A description of the desired solution to a problem" (See https://www.designsociety.org/download-publication/29441/the_role_of_a_specification_in_the_design_process_a_case_study/ (PDF))

See Rodgers/Milton, Product Design (p. 72)


TODO
- How will it be used in Foundry



## Making

TODO: Show how making activities can be used across DD



