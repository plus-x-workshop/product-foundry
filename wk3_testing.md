---
layout: page
title: Week 3 - Testing with people
permalink: /week3/
---



# Testing with people


## Context

- There are many kinds of test; testing with people is often key
- Perfect is the enemy of done - do it quickly
- Introduce test design, prioritisation and action


Think broadly about what you could test. May not be possible to test out core aspects of your main product right now:
- Is there another aspect (e..g packaging, distribution, PoS)
- Is there an analogous idea you could test? 
- Are there other product ideas you can test, so you can develop the skill even if it isn't applied right now


## Outcomes

- Had direct experience of user feedback on tangible product prototype
- Learned about the testing loop: identifying a requirement, designing a test, building a prototype and running a test 
- Revisited PDS - e.g. to reprioritise key product requirements
- Developed understanding of testing approaches
- Understand the importance of testing
- Understand the potential of prototypes to generate new solutions



## Resource

### Who's delivering 


With Fluxx TBC

### Materials and tools

%%TODO%%


## Agenda

| Time    | Lead | Activity                                  |
| ------- | ---- | ----------------------------------------- |
| 10.00am | AS   | Review last week                          |
| 10.15am | AS   | Introduction to testing                   |
| 11.00am | TBC  | Activity brief: Identify and design tests |
| 11.30am | All  | Make test artefacts                       |
| 12.30pm | All  | Lunch                                     |
| 1.00pm  | All  | Live testing/build                        |
| 2.00pm  | All  | Finish core session                       |



## Introduction


### Why test?


 Jim Reeves:

Typically, you’re seeking to understand whether your ideas or solutions can deliver what you hope
As we build and test, we learn, iterate, gather data, amass evidence, and incrementally de-risk
 
- To explore / scope / discover?  
- To make an idea tangible / testable?  
- To trial potential technologies?  
- To demonstrate attainment of a TRL?  
- To gather data via a user-test, field trial or pilot? To refine a process / validate performance?


### What questions can you answer by testing?

%%TODO with examples%%

- End-to-end viability (could the whole thing work?)
- Subsystems (does this one part work?)
- Ease of manufacture/assembly
- Operation, ergonomics, UX, consumer acceptance
- Unit testing (expected outputs for pre-specified inputs)
- Aesthetic qualities (acoustics, materials)
- Strength, durability or other material properties
- Field testing (long-term reliability and wear)
- Supply chain (how does this thing survive in the mail/retail)
- Price/market
- Specific safety/regs standards
- Post-use: compostability, recyclability, disassembly, repair, etc.
- Concept selection (Rodgers p.106) <-- TODO review this
- etc.


### Fast or slow / cheap or expensive?


Making it easy (hallway testing, first 5 users)
When it's expensive to do (eg specialist facilities, diminishing returns, speed to market)
When it's expensive not to do (eg manufacturability)

Not all tests have to be as structured and scientific as this. Can also be creative experiments.


### Core features of most tests

- A hypothesis
- A metric, with a pass/fail condition
- A test subject (prototype, landing page, product sample, demo experience)
- An input (a scenario, a signal, a user, an event)
- A plan for the experiment (how to do, it how long, who with)



### Test artefacts - "prototypes"

- Prototypes are things to be tested. 
- They should be designed against tests, which should be designed to answer questions. 
- Prototypes are not (usually) models, which are used as communication tools.
- They are disposable, and should be as cheap as possible.
- They're not complete: e..g 'looks like' vs 'works like' prototypes

 - [Apple iPhone prototype](https://arstechnica.com/gadgets/2013/03/exclusive-super-early-iphone-prototype-had-5x7-screen-serial-port/)
 - [Palm pilot wooden block](https://www.wired.com/1999/10/the-philosophy-of-the-handheld/)


### Choosing a hypothesis

You don't have to test everything. Pick one thing ("Identify a Variable", from the [D.school Bootleg](https://dschool.stanford.edu/resources/design-thinking-bootleg))

Ways to prioritise

- Risk: what's the things that most jeopardises the product (2 x 2 matrix: Impact and uncertainty - you want to test the risky assumptions (top right))
- Fundamental technology: what's the choice that will drive other decisions
- Pain today: what's holding you back or causing pain right now
- Most exciting potential: but be careful you don't use this as an excuse to just work on the fun stuff


Examples:

- Experience: "Children aged 5-7 will enjoy playing with our talking teddy for at least 20 minutes"
- Function: "Our internet-connected bird feeder will survive a 2 hour rain shower"
- Value: "Construction site managers will pay £100/person/month for our PPE service"

### Designing tests

%%TODO%%

See [Stategyzer test card](assets/the-test-card.pdf):

![](the-test-card.png)


### Making prototypes

%%TODO%%



### Collecting insight and data

From the [D.school Bootleg](https://dschool.stanford.edu/resources/design-thinking-bootleg)

> Feedback Capture Matrix 
> 
> Section off a blank page or whiteboard into quadrants. Draw a plus sign in the upper left quadrant, a delta sign in the upper right quadrant, a question mark in the lower left quadrant, and a light bulb in the lower right quadrant. Fill in the matrix as you give or receive feedback. Place things one likes or finds notable in the upper left (plus sign). Constructive criticism goes in the upper right (delta sign). Questions raised go in the lower left (question mark). And new ideas spurred go in the lower right (light bulb). If you’re giving feedback, strive to give input in each quadrant (especially the upper two: “likes” and “wishes”).



## Activity

%%TODO%%

Choose a hypothesis that you can make progress on today

Either:
- A simple test and artefact you can test here today
- A more complex test and artefact you can test later


```
## Strategies (I've used):

Minimum viable end-to-end hack
- What 's the quickest, easiest way. i can test out the whole workflow/experience from end to end using perhaps existing tech/materials
- Often borrows existing tech as stepping stone
- Allows you to learn about a problem area or technology
- What can you make in 30 mins/1 day /1 week
- You can make use of things you already know work (so you're confident that what you're trying to do is possible)

Subsystem testing
- Break out one part of the whole to test that ("can I get the machine to move at ll in one axis?")
- Can use existing tech as proxy for the rest, or just put aside for now
- Can also make samplers of small parts - eg materials, or UI elements


```



## Reading/watching list

[Design links](../links-design)

