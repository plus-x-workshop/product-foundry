# Design Process Links



## Design Methods and Processes

University of Brighton Product Design
Student info on PD processes: 
https://fixperts.brightonproduct.design/design-projects-and-process/design-process


[D.School Bootleg](https://dschool.stanford.edu/resources/design-thinking-bootleg)

>The Design Thinking Bootleg is a set of tools and methods that we keep in our back pockets. ... We think of these cards as a set of tools/methods that constantly evolves. Within the tools you’ll find tangible examples that take you to action. Anyone that is interested in design thinking can use it for inspiration when you get stuck or to generate new ideas for potential ways of doing things.

[Loughborough University Design Practice Research Group ID Cards](https://www.lboro.ac.uk/microsites/lds/dprg-projects/process_id_cards.html)
[PDF Download](https://www.idsa.org/sites/default/files/IDSA%20iD%20Cards.pdf)


> A taxonomy of design representations to support communication and understanding during new product development



[IDEO Method Cards](https://artdata.co.uk/book/18225/)
[PDF download](https://hcitang.org/uploads/Teaching/ideo-method-cards-2by1.pdf)
A set of user research and prototyping tools


[Innovate.Engineer Physical product development process](https://innovate.engineer/#home)
Mapping prototyping tools onto a 'double diamond' design process.

[FUNdaMentals of Design](https://meddevdesign.mit.edu/fundamentals-of-design/)
A design course focusing on engineering basics from MITs Medical Device Design course

[An Inventor's Quest for the NHL](https://surjan.substack.com/p/an-inventors-quest-for-the-nhl-pt?utm_source=url)

> Learning to design and build new things in "the most undisciplined, irreverent, and original manner possible".

A great series following one engineers efforts to prototype a new face shield for ice hockey players






#### Design Books

Universal Principles of Design
https://www.amazon.co.uk/Universal-Principles-Design-Revised-Updated/dp/1592535879/ref=sr_1_1?crid=KPDL3Y7QAW10&dchild=1&keywords=universal+principles+of+design&qid=1629212901&sprefix=universal+principles+o%2Caps%2C148&sr=8-1

Prototyping and Model Making for Product Design


Research Methods for Product Design





## Briefs and Specification

Rodgers/Milton, Product Design (p, 65, 72)



## Testing


### [Testing with Humans: How to use experiments to drive faster, more informed decision making](https://www.amazon.co.uk/dp/0990800938)
Giff Constable, Frank Rimalovski


p. 13
Start by talking to customers and find out the critical factors - and what are the concerns they have when you talk about your idea.
Assuming you think you can meet these challenges, now you have some hypotheses to test. 

p. 16
Although it sounds counter-intuitive, you can fake the most critical part of your technology to test if your proposition actually makes sense - "wizard-of-oz prototyping"

Designing a test:
- A hypothesis
- A metric, with a pass/fail threshold
- A participant
- How many
- How will you get them
- A plan for the experiment (how to do, it how long)
- Are there other things we want to learn in this experiment?


Also see [Strategyzer's Test Card](https://www.strategyzer.com/blog/posts/2015/3/5/validate-your-ideas-with-the-test-card) (note this is based on 'Lean Startup' methodology)

![](assets/the-test-card.png)

p. 20
"_No plan survives first contact with the enemy_"

Your test will not go as you expect. 
You may not even be able to collect the data you expected to get
You users may be unimpressed with your amazing breakthrough. 
They might hold it upside down, or get excited about something you think irrelevant.

Tests are iterative, just like prototypes


p. 30

Prioritising assumptions and risks:

2 x 2 matrix: Impact and uncertainty - you want to test the risky assumptions (top right)

p. 35 
Test archteypes

- Landing page or response advertising
- Pre-selling
- Paper mockups (interface or data)
- Product prototype, pilot
- "Wizard of Oz" (hidden) or "Concierge" (open)



p.39 
Test design detail


### A hypothesis

- Experience: "Children aged 5-7 will enjoy playing with our talking teddy for at least 20 minutes"
- Function: "Our webcam-enabled bird feeder will survive a 2 hour rain shower"
- Value: "Construction site managers will pay £100/person/month for our PPE service"


### A metric, with a pass/fail threshold

Difficult to pick something that seems arbitrary. 
This is not science; you could do some modelling of outcomes, but likely still an educated guess, e.g. 

- "70% of children will continue playing with the toy for 20 minutes"
- "The device will remain fully operational after 2 hours in a vertical water shower environment"
- "10% of landing page visitors will sign up to our monthly plan"




### Participants (if you're testign with people)

Customers
Exclude edge cases?
Pick the most important segment?
Use whoever's easiest to get hold of
Consider ethics and vulnerable groups
How are yuo going to select the best, if you're openly recruiting

#### How many
Do it in phases

#### How will you get them
- Email list
- Research prime targets
- Personal network
- Approaching strangers in situ (street, office, hallway, park, etc.)
- Online ads
- Embedding experiment into existing product experience

### A plan for the experiment (how to do, it how long)

Consider ethics (and their experience) if working with people
How are you going to get your data
How are you going to manage any sleight of hand

### Are there other things we want to learn in this experiment?

How to collect that data


 
## Coming up with Experiment ideas
p. 53


Just like other idea generation techniques - e. g. Crazy 8s

As a team, do this, with prompts:
- What can we do in one hour, one day, one month
- What exiting tech and materials can we re-use?
- What are the boring or crazy-sounding tests

Each person shares their results without debate
Then dot voting
Choose the top results
Refine

## Looking at the results and making decisions

p. 59

Consider how truthy the experiment is likely to be
From https://giffconstable.com/2013/06/the-truth-curve/

![](assets/truth-curve.png)

Check your biases:

- Anchoring on one piece of information
- Confirmation bias
- Bandwagon effect
- Sunk cost fallacy



### Ideology and Business vs Product
(not in this book)

Testing methods are rooted in ideologies, rarely stated. Lots of business model type tests value 'product-market fit' as the ultimate goal. 

But that is an ideological position (if people want to buy it, then it is worth making), that might lead you to make all sorts of troublesome products: cigarettes; candy crush; hummers

Also need to be aware of the differnece between the business and the product or technology.
We're focused on products and technooogy here, but sales and pricing, or distribution channels might be critical success factors for you.

(And these can influence product design, so important to consider even here)





### [D.School Bootleg](https://dschool.stanford.edu/resources/design-thinking-bootleg)

Identify a Variable

> Rather than prototype a complete mock- up of a solution, it’s more productive to single out and test a specific variable. Identifying a variable not only saves time and money (you don’t need to create all facets of a complex solution), it gives you the opportunity to test multiple prototypes, each varying in the one property. This encourages the user to make nuanced comparisons between prototypes and choose one option over another.   
> 
> How to identify a variable 
> Prototype with a purpose. Based on user needs and insights, identify one variable of your concept to flesh out and test. Then build a few iterations. Keep prototypes as low resolution as possible. 
> 
> Remember, a prototype doesn’t have to be, or even look like the solution. You might want to know how heavy a device should be. Create prototypes of varied weight, without making them operable. You may want to find out if users prefer delivery versus pick up. Build boxes for each service, without filling them. By selecting one variable to test, you can bring resolution to that aspect of your concept and slide one step closer to a design solution.


Wizard of Oz Prototyping 

> A wizard of Oz prototype fakes functionality that you want to test with users, saving you the time and money of actually creating it. Wizard of Oz prototypes often refer to prototypes of digital systems, in which the user thinks the response is computer-driven, when in fact it’s human-controlled.
> 
> How to create a Wizard of Oz prototype: 
> 
> Determine what you want to test. Then figure out how to fake that functionality and still give users an authentic experience. You can combine existing tools (tablets, email systems, Powerpoint) with human intervention to create the illusion of functionality. 
> 
> The company Aardvark needed to test user reactions to their online interface for connecting people with questions to qualified people with answers. Instead of coding, they used an instant messaging system and team members behind the scenes to physically route questions and answers to the right people. 
> 
> Wizard of Oz prototypes can be extended beyond the digital realm, to physical prototypes. You could prototype a vending machine without building the mechanics behind it—using a person hidden inside to deliver purchases.


User testing     

> Testing with users is a fundamental part of human-centered design. You test with users to not only refine your solution, but to better understand the people you’re designing for. When you test prototypes, consider both what you can learn about your solution and what you can learn about your user—you can always use more empathy.     
> 
> How to test with users:  
> 
> Let your user experience the prototype.   
> Show don’t tell. Put your prototype in the user’s hands (or your user in the prototype) and give only the basic context they need to understand what to do.   
> 
> Have them talk through their experience.   
> Use prompts. “Tell me what you’re thinking as you do this.”  
> 
> Actively observe.   
> Don’t immediately “correct” your user. Watch how they use (and misuse) your prototype.   
> 
> Follow up with questions.   
> This is often the most valuable part. “Show me why this would (not) work for you.” “Can you tell me how this made you feel?” “Why?” Answer questions with questions. “Well, what do you think that button does?”
> 


