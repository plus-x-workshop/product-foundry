# Introduction to Prototyping (Icebreaker making exercise)


## Why

- Research and testing shouldn't be a big barrier - small tests are better than no tests
- Playing with the idea can generate new routes to explore (a form of research)
- People feel free to criticise a rubbish looking model – less so a beautiful model
- Don't waste time pursuing fruitless approaches
- Don't get too attached to ideas you have invested in
- Stop jumping to hi-fidelity tools (beautifully realising your fully formed idea)


Also:
- We want to get started making
- We want to generate ideas and test relationship between hands and head.


## What are we going to do

Make a rough model (a mockup) in response to a brief.

#### Timings

| Activity                                  | Time    |
| ----------------------------------------- | ------- |
| Introduction to briefs and specifications | 20 mins |
| Simple prototyping methods and tools      | 20 mins |
| Present and interrogate the brief         | 20 mins |
| Make                                      | 60 mins |




Instruct: 20 mins
Make: 30 mins
Present: 10 mins
Review: 20 mins



What is a 'mockup'?
- Minimum viable prototype
- A few minutes or hours to make
- Disposable materials
- Something you can share


What can you learn?
- Overall size/form/mass
- Ergonomics, hand or body interaction
- Screen UI layouts and flows
- Basic mechanism functions
- In-situ performance (e.g. point of sale, distribution, in home use)


### Responding to a brief

TODO: A sample brief

- A set of requirements
- Design is to some extent the refinement of requirements
- Requirements can be internal to you (e.g. values, aspirations for the project or your career)
- Or external (what the user needs or the business model)
- Requirements can influence each other (your aspirations can influence business model, or users you target)


Interrogate the brief

- What does it mean
- What are our requirements
- What's negotiable
- What's been said explicitly, what's only implicit?


https://fixperts.brightonproduct.design/design-projects-and-process/briefs-and-specifications


#### Brief

Work in teams to respond to a sci-fi/speculative brief, e.g.

-   Make an enchanted object that gives you a magical power you could use in everyday life
-   Make a mode of transport to help you get around/work/shop/play in a future when Brighton is under the sea


Maybe brief needs to give more input - so not thinking up an idea out of thin air

Teams - could make construction difficult...


**Materials and tools**

See M3 Design Mockup Guide ([PDF](assets/Mockup-Guide.pdf))


Basic card only
- Corrugated card
- Old packaging/boxes
- Paper/thin card 
- Acetate/fabric/cork sheet or other materials
- Hobby/craft/ detail knife
- Glue/hot glue/spray adhesive
- Cutting mat
- Eye protection


Step up
- Material board
- Insulation polyurethane foamboard
- Modelling polyurethane foam
- Chisels/files/rasps
- Magnets
- Hand tools (e.g. saws)
- Printer (or pens and paper) for on-screen UIs
- Greeblies and Frankenstein source material (bottle lids, handles, watch straps, tables, etc, etc.)

Interactivity
- Littlebits
- Arduino and modules
- Microbit


Large scale or specific functionality
- 3D printer
- Power tools (drill, band saw, etc.)
- Off-the-shelf components
- Hardware fasteners
- Plywood/timber


**Safety**

- Knife use
- Eye protection
- Glues (and hot glue guns)


#### Basic methods

Demos - or images of

- Card and hot glue
- Foam model
- Frankenstein model
- Stickers on boxes
- In-situ environment model
- Mechanism (e.g. delrin flexures)

