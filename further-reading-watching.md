# Links to books, articles and videos

Contents



- [[#Design Methods and Process|Design Methods and Process]]
	- [[#Design Methods and Process#Links|Links]]
	- [[#Design Methods and Process#Design Books|Design Books]]
	- [[#Design Methods and Process#Specific topics|Specific topics]]
		- [[#Specific topics#Briefs and Specification|Briefs and Specification]]
- [[#CAD and 3D modelling|CAD and 3D modelling]]
	- [[#CAD and 3D modelling#Books|Books]]
	- [[#CAD and 3D modelling#Videos|Videos]]
- [[#Manufacturing / DFMA|Manufacturing / DFMA]]
	- [[#Manufacturing / DFMA#Books|Books]]
	- [[#Manufacturing / DFMA#Videos|Videos]]
- [[#Materials|Materials]]
	- [[#Materials#Links|Links]]
- [[#Ideation and Creative Thinking|Ideation and Creative Thinking]]


## Design Methods and Process 

### Links

**University of Brighton Product Design**

Student info on PD processes: 
https://fixperts.brightonproduct.design/design-projects-and-process/design-process


**[D.School Bootleg](https://dschool.stanford.edu/resources/design-thinking-bootleg)**

>The Design Thinking Bootleg is a set of tools and methods that we keep in our back pockets. ... We think of these cards as a set of tools/methods that constantly evolves. Within the tools you’ll find tangible examples that take you to action. Anyone that is interested in design thinking can use it for inspiration when you get stuck or to generate new ideas for potential ways of doing things.

**[Loughborough University Design Practice Research Group ID Cards](https://www.lboro.ac.uk/microsites/lds/dprg-projects/process_id_cards.html)** [PDF Download](https://www.idsa.org/sites/default/files/IDSA%20iD%20Cards.pdf)

> A taxonomy of design representations to support communication and understanding during new product development

**[IDEO Method Cards](https://artdata.co.uk/book/18225/)** [PDF download](https://hcitang.org/uploads/Teaching/ideo-method-cards-2by1.pdf)

A set of user research and prototyping tools

**[Innovate.Engineer Physical product development process](https://innovate.engineer/#home)**

Mapping prototyping tools onto a 'double diamond' design process.

**[FUNdaMentals of Design](https://meddevdesign.mit.edu/fundamentals-of-design/)**

A design course focusing on engineering basics from MITs Medical Device Design course

**[An Inventor's Quest for the NHL](https://surjan.substack.com/p/an-inventors-quest-for-the-nhl-pt?utm_source=url)**

> Learning to design and build new things in "the most undisciplined, irreverent, and original manner possible".

A great series following one engineers efforts to prototype a new face shield for ice hockey players




### Design Books


[Prototyping and Model Making for Product Design](https://www.amazon.co.uk/Prototyping-Modelmaking-Product-Design-Second/dp/1786275112/ref=sr_1_3?crid=1JD7G3DGQUFZK&keywords=Prototyping+and+Model+Making+for+Product+Design&qid=1653297994&s=books&sprefix=prototyping+and+model+making+for+product+design%2Cstripbooks%2C196&sr=1-3), Bjarki Hallgrimsson


[Research Methods for Product Design](https://www.amazon.co.uk/Research-Methods-Product-Design-Portfolio/dp/1780673027), Alex Milton and Paul Rodgers 

[The Fundamentals of Product Design](https://www.amazon.co.uk/Fundamentals-Product-Design-Richard-Morris/dp/1472578244/ref=pd_lpo_1?pd_rd_i=1472578244&psc=1), Richard Morris

[Product Design](https://www.amazon.co.uk/Product-Design-Portfolio-Alex-Milton/dp/1856697517/ref=tmm_pap_swatch_0?_encoding=UTF8&qid=&sr=), Alex Milton and Paul Rodgers







### Specific topics

#### Briefs and Specification

Rodgers/Milton, Product Design (p, 65, 72)





## CAD and 3D modelling


### Books

**Fusion 360 for Absolute Beginners**



### Videos

**[Fusion 360 For Beginners - Recorded Webinar](https://www.youtube.com/watch?v=VbSkwvZyU_0)**

This session is to help you learn the basics in Fusion 360. This is intended for those out there who are completely new users of Fusion 360 and 3D design. After an introduction to the user interface, time will be spent learning how to navigate, model with primitives, model from sketches, and assembly design/joints will be covered.


**Blender Doughnut tutorial**



## Manufacturing / DFMA


### Books

[Rob Thompson: Manufacturing Processes for Design Professionals](https://www.amazon.co.uk/dp/0500513759/ref=pe_27063361_487055811_TE_dp_1)
General text on a wide variety of manufacturing processes

[Rob Thompson:  The Materials Sourcebook for Design Professionals](https://www.amazon.co.uk/Materials-Sourcebook-Design-Professionals/dp/0500518548/ref=sr_1_1?crid=20MGBAWWJURYW&keywords=materials+sourcebook&qid=1645186829&s=books&sprefix=materials+sourcebook%2Cstripbooks%2C47&sr=1-1)
Similar, but more of a focus on material selection

[Andrew Bunnie Huang: The Hardware Hacker](https://www.amazon.co.uk/Hardware-Hacker-Andrew-Huang/dp/159327758X/ref=sr_1_3?crid=1KNWZ4V71JEC2&dchild=1&keywords=Bunny+Huang&qid=1635419902&s=books&sprefix=bunny+huang%2Cstripbooks%2C96&sr=1-3)
Great insight on manufacturing electronics products in China

[K. Swift, Manufacturing Process Selection Handbook](https://www.amazon.co.uk/Manufacturing-Process-Selection-Handbook-Swift/dp/0080993605/ref=sr_1_1?crid=2YQSFGPGQQ24E&keywords=manufacturing+process+selection&qid=1646997532&s=books&sprefix=manufacturing+process+selection%2Cstripbooks%2C72&sr=1-1)
Heavy focus on DMA principles and cost calculations

[Otto and Wood: Product Design: Techniques in Reverse Engineering and Product Development](https://www.amazon.co.uk/Product-Design-Kevin-Otto/dp/0130212717)
Chapter 6, p. 197 onwards

[Bryan Bergeron:  Teardowns: Learn How Electronics Work by Taking Them Apart](https://www.amazon.co.uk/Teardowns-Learn-Electronics-Taking-Apart/dp/0071713344)
On teardowns of electronic devices

[Todd McLellan: Things Come Apart 2.0: A Teardown Manual for Modern Living](https://www.amazon.co.uk/Things-Come-Apart-2-0-Teardown/dp/0500294879)
More visual teardowns

[Alex Milton and Paul Rodgers: Research Methods for Product Design](https://www.amazon.co.uk/Research-Methods-Product-Design-Portfolio/dp/1780673027)
p. 33 Product Autopsy



### Videos

[Make Works videos](https://make.works/about)

Teardowns:
- [Instrumental](https://instrumental.com/resources/teardown/)
- [Lego minifig scans](https://scanofthemonth.com/LEGO-Minifig/) 
- [Whats inside a robotic KITTEN?](https://www.youtube.com/watch?v=zcX_H8X6ga0)
- [M3 Design Teardowns Playlist](https://www.youtube.com/playlist?list=PLX_QtVuugdKFaOlFUKwkSPV0pLQuagoyq)
- [Framework Laptop Teardown](https://www.ifixit.com/News/51614/framework-laptop-teardown-10-10-but-is-it-perfect)
- [Sony A1 Complete Disassembly And Teardown - Kolari Vision 2022](https://kolarivision.com/sony-a1-complete-disassembly-and-teardown/)


## Materials 

### Links

[Thinking Materials]() (http://www.thinkingmaterials.net/index.php/cards/)

[PDF Download](http://www.thinkingmaterials.net/Thinking_Materials_Cards.pdf)

>The Thinking Materials design tool identifies creative product opportunities that use indigenous materials and crafts as an alternative to more conventional manufacturing processes. It demonstrates the potential for a supply chain in which an emerging economy is always the country of origin (COO) for the manufacture of a product and/or growth/production of materials.



[Ansys Granta EduPack](https://www.ansys.com/en-gb/products/materials/granta-edupack)

>Ansys Granta EduPack—formerly CES EduPack—is a unique set of teaching resources that help academics enhance courses related to materials across engineering, design, science and sustainable development.

[Materiom](https://materiom.org)

>Materiom provides open source recipes and data on materials made from abundant sources of natural ingredients, like agricultural waste.

## Ideation and Creative Thinking

**[Unnecessary Inventions](https://unnecessaryinventions.com/about/)**

> Unnecessary Inventions is the brainchild of inventor and evil genius Matty Benedetto. Over the past two years, he has designed and fully prototyped over 280 new inventions that solve problems that don't exist.

> Each week from the Unnecessary Design Studio in Burlington Vermont, Matty develops 1-3 brand new fake consumer products using diverse production methods from 3D printing, sewing, mold making, wood working, laser cutter, and whatever else he can get his hands on

**[The reinvention of Normal](http://dominicwilcox.com/?portfolio=the-reinvention-of-normal)**
